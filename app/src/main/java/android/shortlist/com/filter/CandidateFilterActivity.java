package android.shortlist.com.filter;

import android.content.Intent;
import android.os.Bundle;
import android.shortlist.com.R;
import android.shortlist.com.adapter.FilterCommonAdapter;
import android.shortlist.com.adapter.JobTypeAdapter;
import android.shortlist.com.data.FilterDataTest;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class CandidateFilterActivity extends AppCompatActivity {
    private RecyclerView rvDataPosted,rvCompanyName,rvJobType;
    private List<FilterDataTest> dataTestList = new ArrayList<>();
    private FilterCommonAdapter adapter;
    private JobTypeAdapter jobTypeAdapter;
    private TextView gotoFilterCompany;
    private ImageView ivCloseFilter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.candidate_filter_activity);
        rvDataPosted = findViewById(R.id.rv_data_posted);
        rvCompanyName = findViewById(R.id.rv_company);
        rvJobType = findViewById(R.id.rv_job_type);
        gotoFilterCompany = findViewById(R.id.tv_add_filter_company);
        initToolbar();
        init();
        initListener();
    }

    private void initListener() {
        gotoFilterCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CandidateFilterActivity.this, CompanyFilterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_filter);
//        setSupportActionBar(toolbar);
        ivCloseFilter = toolbar.findViewById(R.id.iv_close_filter);

        ivCloseFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init(){
        LinearLayoutManager linearLayoutManagerCompany = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCompanyName.setLayoutManager(linearLayoutManagerCompany);

        LinearLayoutManager linearLayoutManagerDatePosted = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvDataPosted.setLayoutManager(linearLayoutManagerDatePosted);
        adapter = new FilterCommonAdapter(CandidateFilterActivity.this);
        rvDataPosted.setAdapter(adapter);

        LinearLayoutManager linearLayoutManagerJobType = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvJobType.setLayoutManager(linearLayoutManagerJobType);
        jobTypeAdapter = new JobTypeAdapter(CandidateFilterActivity.this);

        rvDataPosted.setAdapter(adapter);
        rvJobType.setAdapter(jobTypeAdapter);
        setDataJobPosted();
        setJobType();
    }

    private void setDataJobPosted(){
        FilterDataTest dataTest = new FilterDataTest();
        dataTest.setTitle("Past 24 Hours");
        dataTest.setStatus(false);
        dataTestList.add(dataTest);

        FilterDataTest dataTest2 = new FilterDataTest();
        dataTest2.setTitle("Past Week");
        dataTest2.setStatus(false);
        dataTestList.add(dataTest2);

        FilterDataTest dataTest3 = new FilterDataTest();
        dataTest3.setTitle("Past Month");
        dataTest3.setStatus(true);
        dataTestList.add(dataTest3);

        adapter.setData(dataTestList);
    }

    private void setJobType(){
        dataTestList = new ArrayList<>();
        FilterDataTest dataTest = new FilterDataTest();
        dataTest.setTitle("Full-time");
        dataTest.setStatus(false);
        dataTestList.add(dataTest);

        FilterDataTest dataTest2 = new FilterDataTest();
        dataTest2.setTitle("Part-time");
        dataTest2.setStatus(false);
        dataTestList.add(dataTest2);

        FilterDataTest dataTest3 = new FilterDataTest();
        dataTest3.setTitle("Internship");
        dataTest3.setStatus(false);
        dataTestList.add(dataTest3);

        FilterDataTest dataTest4 = new FilterDataTest();
        dataTest4.setTitle("Contract");
        dataTest4.setStatus(false);
        dataTestList.add(dataTest4);

        jobTypeAdapter.setData(dataTestList);
    }
}
