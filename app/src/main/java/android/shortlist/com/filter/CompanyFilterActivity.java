package android.shortlist.com.filter;

import android.os.Bundle;
import android.shortlist.com.R;
import android.shortlist.com.adapter.CompanyFilterAdapter;
import android.shortlist.com.adapter.SuggestionCompanyAdapter;
import android.shortlist.com.data.FilterDataTest;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class CompanyFilterActivity extends AppCompatActivity {
    private RecyclerView rvFilterCompany, rvSuggestionCompany;
    private Toolbar toolbarFilter;
    private CompanyFilterAdapter adapter;
    private SuggestionCompanyAdapter suggestionCompanyAdapter;
    private List<FilterDataTest> dataTestList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_filter);
        rvFilterCompany = findViewById(R.id.id_rv_filter_company);
        rvSuggestionCompany = findViewById(R.id.id_rv_suggestion_company);
        initToolbar();
        init();
        setDataFilterCompany();
        setDataSuggestionCompany();
    }

    private void setDataSuggestionCompany() {
        dataTestList = new ArrayList<>();
        FilterDataTest dataTest = new FilterDataTest();
        dataTest.setTitle("APPLE Inc");
        dataTest.setStatus(false);
        dataTestList.add(dataTest);

        FilterDataTest dataTest2 = new FilterDataTest();
        dataTest2.setTitle("MICROSOFT");
        dataTest2.setStatus(false);
        dataTestList.add(dataTest2);

        FilterDataTest dataTest3 = new FilterDataTest();
        dataTest3.setTitle("HONDA");
        dataTest3.setStatus(false);
        dataTestList.add(dataTest3);

        FilterDataTest dataTest4 = new FilterDataTest();
        dataTest4.setTitle("TOKOPEDIA");
        dataTest4.setStatus(false);
        dataTestList.add(dataTest4);

        suggestionCompanyAdapter.setData(dataTestList);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_filter_company);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final SearchView searchView = toolbar.findViewById(R.id.sv_filter);
        searchView.setFocusable(true);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.requestFocus();
            }
        });
        searchView.onActionViewExpanded();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {
        LinearLayoutManager linearLayoutManagerCompany = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvFilterCompany.setLayoutManager(linearLayoutManagerCompany);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvSuggestionCompany.setLayoutManager(mLayoutManager);

        adapter = new CompanyFilterAdapter();
        suggestionCompanyAdapter = new SuggestionCompanyAdapter();

        rvFilterCompany.setAdapter(adapter);
        rvSuggestionCompany.setAdapter(suggestionCompanyAdapter);
    }

    private void setDataFilterCompany(){
        FilterDataTest dataTest = new FilterDataTest();
        dataTest.setTitle("GOJEK");
        dataTest.setStatus(false);
        dataTestList.add(dataTest);

        FilterDataTest dataTest2 = new FilterDataTest();
        dataTest2.setTitle("GOOGLE");
        dataTest2.setStatus(false);
        dataTestList.add(dataTest2);

        FilterDataTest dataTest3 = new FilterDataTest();
        dataTest3.setTitle("FACEBOOK");
        dataTest3.setStatus(false);
        dataTestList.add(dataTest3);

        FilterDataTest dataTest4 = new FilterDataTest();
        dataTest4.setTitle("AMAZONE");
        dataTest4.setStatus(false);
        dataTestList.add(dataTest4);

        adapter.setData(dataTestList);
    }
}
