package android.shortlist.com.adapter;

import android.content.Context;
import android.shortlist.com.R;
import android.shortlist.com.data.FilterDataTest;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FilterCommonAdapter extends RecyclerView.Adapter<FilterCommonAdapter.ItemFilter> {

    private List<FilterDataTest> dataTestList = new ArrayList<>();
    private Context context;

    public FilterCommonAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<FilterDataTest> dataTestList) {
        this.dataTestList = dataTestList;
        notifyDataSetChanged();
    }

    public void clearData(){
        this.dataTestList.clear();
    }

    @NonNull
    @Override
    public ItemFilter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = null;
        layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_common_filter, parent, false);
        return new ItemFilter(layoutView, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemFilter holder, int position) {
        holder.setData(dataTestList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataTestList.size();
    }

    public static class ItemFilter extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView imageView;
        private Context context;

        public ItemFilter(View itemView, Context context) {
            super(itemView);
            this.context = context;
            title = itemView.findViewById(R.id.tv_filter_title);
            imageView = itemView.findViewById(R.id.id_checklist_item_filter);
        }

        public void setData(FilterDataTest data) {
            title.setText(data.getTitle());

            if (data.isStatus()) {
                imageView.setVisibility(View.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    title.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selector_common_active_item));
                } else {
                    title.setBackground(ContextCompat.getDrawable(context, R.drawable.selector_common_active_item));
                }
            } else {
                imageView.setVisibility(View.GONE);
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    title.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.item_job_border));
                } else {
                    title.setBackground(ContextCompat.getDrawable(context, R.drawable.item_job_border));
                }
            }
        }
    }
}
