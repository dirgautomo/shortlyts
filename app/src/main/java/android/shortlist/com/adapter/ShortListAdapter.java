package android.shortlist.com.adapter;

import android.content.Context;
import android.shortlist.com.R;
import android.shortlist.com.data.TestLocalData;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ShortListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int USER_CHAT = 1;
    public static final int MY_CHAT = 2;
    public static final int JOB_INFO = 3;
    public static final int JOB_RECOMENDED = 4;
    public static final int USER_IMAGE_LAYOUT = 5;
    public static final int LOADING_BAR = 6;
    private List<TestLocalData> data = new ArrayList<>();
    private Context context;

    public ShortListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<TestLocalData> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = null;
        int viewId;
        switch (viewType) {
            case USER_CHAT:
                viewId = R.layout.item_layout_left;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new LeftViewHolder(layoutView);
            case MY_CHAT:
                viewId = R.layout.item_layout_right;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new RightViewHolder(layoutView);
            case JOB_INFO:
                viewId = R.layout.view_item_info_job;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new JobInfo(layoutView);
            case JOB_RECOMENDED:
                viewId = R.layout.view_item_job_recomended;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new JobRecomended(layoutView);
            case USER_IMAGE_LAYOUT:
                viewId = R.layout.item_layout_cicle;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new ImageUser(layoutView);
            case LOADING_BAR:
                viewId = R.layout.item_layout_loading_bar;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new ImageUser(layoutView);
            default:
                viewId = R.layout.view_item_job_recomended;
                layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
                return new JobRecomended(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case USER_CHAT:
                LeftViewHolder userChat = (LeftViewHolder)holder;
                userChat.setTextChat(data.get(position).getText());
               break;
            case MY_CHAT:
                break;
            case JOB_INFO:
                break;
            case JOB_RECOMENDED:
                break;
            case USER_IMAGE_LAYOUT:
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    public static class LeftViewHolder extends RecyclerView.ViewHolder {
        TextView textChat;
        public LeftViewHolder(View itemView) {
            super(itemView);
            if (itemView != null){
                textChat = itemView.findViewById(R.id.tv_left_chat);
            }
        }

        public void setTextChat(String textChat) {
            this.textChat.setText(textChat);
        }
    }

    public static class RightViewHolder extends RecyclerView.ViewHolder {
        public RightViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class JobRecomended extends RecyclerView.ViewHolder {
        public JobRecomended(View itemView) {
            super(itemView);
        }
    }

    public static class ImageUser extends RecyclerView.ViewHolder {
        public ImageUser(View itemView) {
            super(itemView);
        }
    }

    public static class LoadingBar extends RecyclerView.ViewHolder {
        public LoadingBar(View itemView) {
            super(itemView);
        }
    }

    public static class JobInfo extends RecyclerView.ViewHolder {
        public JobInfo(View itemView) {
            super(itemView);
        }
    }
}
