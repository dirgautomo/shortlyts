package android.shortlist.com.adapter;

import android.shortlist.com.R;
import android.shortlist.com.data.FilterDataTest;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SuggestionCompanyAdapter extends RecyclerView.Adapter<SuggestionCompanyAdapter.ItemHolderSuggestionCompany> {
    private List<FilterDataTest> data = new ArrayList<>();

    public void setData(List<FilterDataTest> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SuggestionCompanyAdapter.ItemHolderSuggestionCompany onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = null;
        int viewId;
        viewId = R.layout.view_item_suggestion_company;
        layoutView = LayoutInflater.from(parent.getContext()).inflate(viewId, parent, false);
        return new SuggestionCompanyAdapter.ItemHolderSuggestionCompany(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestionCompanyAdapter.ItemHolderSuggestionCompany holder, int position) {
        holder.setData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ItemHolderSuggestionCompany extends RecyclerView.ViewHolder {
        private TextView itemTitle;
        public ItemHolderSuggestionCompany(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.tv_suggestion_filter_company);
        }

        public void setData(FilterDataTest data){
            itemTitle.setText(data.getTitle());
        }
    }
}
