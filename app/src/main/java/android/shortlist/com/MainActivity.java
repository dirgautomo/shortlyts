package android.shortlist.com;

import android.content.Intent;
import android.os.Bundle;
import android.shortlist.com.adapter.ShortListAdapter;
import android.shortlist.com.data.TestLocalData;
import android.shortlist.com.filter.CandidateFilterActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mainList;
    private ShortListAdapter adapter;
    private List<TestLocalData> testLocalDataList = new ArrayList<>();
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private ImageView btnMore;
    private LinearLayout layoutPanel;
    private TextView gotoFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainList = findViewById(R.id.rv_main);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mainList.setLayoutManager(mLayoutManager);
        mainList.setItemAnimator(new DefaultItemAnimator());
        slidingUpPanelLayout = findViewById(R.id.sliding_layout);
        btnMore = findViewById(R.id.btn_more);
        layoutPanel = findViewById(R.id.dragView);
        gotoFilter = findViewById(R.id.panel_filter);
        initValue();
        initListener();
    }

    private void initValue(){
        adapter = new ShortListAdapter(MainActivity.this);
        mainList.setAdapter(adapter);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        setValueAdapter();
    }

    private void setValueAdapter(){
        TestLocalData data1 = new TestLocalData();
        data1.setType(ShortListAdapter.USER_CHAT);
        data1.setText("Hi Dirga, How Are you?");
        testLocalDataList.add(data1);

        TestLocalData data2 = new TestLocalData();
        data2.setType(ShortListAdapter.USER_CHAT);
        data2.setText("What do you want to do today?");
        testLocalDataList.add(data2);

        TestLocalData data3 = new TestLocalData();
        data3.setType(ShortListAdapter.MY_CHAT);
        data3.setText("Give me money...");
        testLocalDataList.add(data3);

        TestLocalData data7 = new TestLocalData();
        data7.setType(ShortListAdapter.USER_IMAGE_LAYOUT);
        data7.setText("");
        testLocalDataList.add(data7);

        TestLocalData data9 = new TestLocalData();
        data9.setType(ShortListAdapter.USER_CHAT);
        data9.setText("i'll recommended you to new position that perfect for your qualification.");
        testLocalDataList.add(data9);

        TestLocalData data4 = new TestLocalData();
        data4.setType(ShortListAdapter.JOB_INFO);
        data4.setText("");
        testLocalDataList.add(data4);

        TestLocalData data5 = new TestLocalData();
        data5.setType(ShortListAdapter.JOB_RECOMENDED);
        data5.setText("");
        testLocalDataList.add(data5);

        TestLocalData data6 = new TestLocalData();
        data6.setType(ShortListAdapter.JOB_RECOMENDED);
        data6.setText("");
        testLocalDataList.add(data6);

        TestLocalData data8 = new TestLocalData();
        data8.setType(ShortListAdapter.LOADING_BAR);
        data8.setText("");
        testLocalDataList.add(data8);

        adapter.setData(testLocalDataList);
    }

    private void initListener(){
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN){
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }else{
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                }
            }
        });

        slidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });

        layoutPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });

        gotoFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CandidateFilterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout != null &&
                (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else {
            super.onBackPressed();
        }
    }
}
